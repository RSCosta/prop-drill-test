INSTALLATION:


git clone git@gitlab.com:RSCosta/prop-drill-test.git fieldChallenge_Ricardo

cd fieldChallenge_Ricardo

npm i

npm run build


npm i serve    (install serve or use any of your prefered server)


serve -s ./build

goto: localhost:5000   (port may vary)

______
USING PROVIDED ZIP

unzip

cd to/folder

npm i serve

serve -s ./build

goto: localhost:5000   (port may vary)



______

YOU CAN ALSO JUST RUN THE FOLLOWING COMMAND AND IT WILL CREATE A LOCAL SERVER AND RUN THE APP (BUT NOT IN PRODUCTION MODE)
Note that the development build is not optimized.

git clone git@gitlab.com:RSCosta/prop-drill-test.git fieldChallenge_Ricardo

(or cd to the given zip)

cd fieldChallenge_Ricardo

npm i

npm start