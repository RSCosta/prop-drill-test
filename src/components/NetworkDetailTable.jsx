// @flow

/* Copyright (C) Ricardo Costa - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Costa <ricardo.simoescosta@hotmail.com>, July 2018
 */

import * as React from 'react'
import Paper from '@material-ui/core/Paper'
import {
  FilteringState,
  PagingState,
  IntegratedFiltering,
  IntegratedPaging,
} from '@devexpress/dx-react-grid'
import {
  Grid,
  Table,
  TableHeaderRow,
  TableFilterRow,
  PagingPanel,
} from '@devexpress/dx-react-grid-material-ui'
import RemoveRedEye from '@material-ui/icons/RemoveRedEye'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal'

const rand = () => {
  return Math.round(Math.random() * 20) - 10
}

const getModalStyle = () => {
  const top = 50 + rand()
  const left = 50 + rand()

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  }
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  }
})

const Cell = props => {
  // const onClick = (href: string, extra) => props.onClick(href, extra)

  if (props.column.name === 'name') {
    return (
      <Table.Cell {...props}>
        <span>{props.value}</span>
        <RemoveRedEye />
      </Table.Cell>
    )
  }
  return <Table.Cell {...props} />
}

class NetworksTable extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      columns: [
        { name: 'name', title: 'Name' },
        { name: 'empty_slots', title: 'Slots' },
        { name: 'free_bikes', title: 'Bikes' }
      ],
      rows: [],
      loading: false,
      pageSizes: [5, 10, 15, 20, 100],
      networks: {},
      isModalOpen: false
    }
  }

  handleClose = () => this.setState({ isModalOpen: false })

  render() {
    const { columns, pageSizes, extra } = this.state
    const { classes, networksDetail } = this.props
    return (
      <React.Fragment>
        {this.props.networksDetail.location && (
          <h1>{this.props.networksDetail.location.city}</h1>
        )}
        <Paper>
          <Grid rows={networksDetail.stations} columns={columns}>
            <FilteringState defaultFilters={[{ columnName: '', value: '' }]} />
            <PagingState defaultCurrentPage={0} pageSize={10} />
            <IntegratedFiltering />
            <IntegratedPaging />
            <Table
              cellComponent={props =>
                React.cloneElement(<Cell {...props} />, {
                  onClick: isModalOpen =>
                    this.setState({ isModalOpen: true, extra: props.row.extra })
                })
              }
            />
            <TableHeaderRow />
            <TableFilterRow />
            <PagingPanel pageSizes={pageSizes} />
          </Grid>
        </Paper>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.isModalOpen}
          onClose={this.handleClose}
          onBackdropClick={this.handleClose}
          onEscapeKeyDown={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            {extra && (
              <React.Fragment>
                <Typography variant="body2" id="modal-title">
                  {extra.uid && `Bike ID: ${extra.uid}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.city && `City: ${extra.city}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.location && `Location: ${extra.location}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.banking && `Banking: ${extra.banking}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.number && `Number: ${extra.number}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.slots && `Slots: ${extra.slots}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.slots && `Status: ${extra.status}`}
                </Typography>
                <Typography variant="body2" id="modal-title">
                  {extra.has_bikes && `Bikes available? ${extra.has_bikes}`}
                </Typography>
              </React.Fragment>
            )}
          </div>
        </Modal>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(NetworksTable)
