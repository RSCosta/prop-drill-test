// @flow

/* Copyright (C) Ricardo Costa - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Costa <ricardo.simoescosta@hotmail.com>, July 2018
 */

import * as React from 'react'
import Paper from '@material-ui/core/Paper'
import {
  FilteringState,
  PagingState,
  IntegratedFiltering,
  IntegratedPaging,
} from '@devexpress/dx-react-grid'
import {
  Grid,
  Table,
  TableHeaderRow,
  TableFilterRow,
  PagingPanel,
} from '@devexpress/dx-react-grid-material-ui'
import RemoveRedEye from '@material-ui/icons/RemoveRedEye'
import type { Networks } from '../utils/types'

import Store from '../utils/store/Store'

import { withRouter, Link } from 'react-router-dom'

const Cell = props => {
  const onClick = (href: string) => props.onClick(href)
  if (props.column.name === 'name') {
    return (
      <Table.Cell {...props}>
        <span>{props.value}</span>
        <Link to={`/networkdetail/${props.row.name}`}>
          <RemoveRedEye onClick={() => onClick(props.row.href)} />
        </Link>
      </Table.Cell>
    )
  }
  return <Table.Cell {...props} />
}

class NetworksTable extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      columns: [
        { name: 'company', title: 'company' },
        { name: 'name', title: 'Name' },
        {
          name: 'country',
          title: 'Country',
          getCellValue: row => (row.location ? row.location.country : undefined)
        },
        {
          name: 'city',
          title: 'City',
          getCellValue: row => (row.location ? row.location.city : undefined)
        }
      ],
      rows: [],
      loading: false,
      pageSizes: [5, 10, 15, 20, 100],
      networks: {}
    }
    // this.changeSelection = selection => this.setState({ selection })
  }

  networksChanged = (allNetworks: Networks) => {
    this.setState({ rows: allNetworks.networks })
  }

  componentDidMount() {
    Store.subscribe(this.networksChanged)
    Store.getNetworks()
  }

  componentWillUnmount() {
    Store.unsubscribe(this.networksChanged)
  }

  render() {
    const { columns, pageSizes, rows } = this.state
    return (
      <Paper>
        <Grid rows={rows} columns={columns}>
          <FilteringState defaultFilters={[{ columnName: '', value: '' }]} />
          <PagingState defaultCurrentPage={0} pageSize={10} />
          <IntegratedFiltering />
          <IntegratedPaging />
          <Table
            cellComponent={props =>
              React.cloneElement(<Cell {...props} />, {
                onClick: href => this.props.click(href)
              })
            }
          />
          <TableHeaderRow />
          <TableFilterRow />
          <PagingPanel pageSizes={pageSizes} />
        </Grid>
      </Paper>
    )
  }
}

export default withRouter(NetworksTable)
