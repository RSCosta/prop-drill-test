/* Copyright (C) Ricardo Costa - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Costa <ricardo.simoescosta@hotmail.com>, July 2018
 */

import React, { Component } from 'react'
import './App.css'
import Store from './utils/store/Store'
import NetworksTable from './components/NetworksTable'
import NetworkDetailTable from './components/NetworkDetailTable'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      networksDetail: {
        stations: [{}]
      }
    }
  }

  networksChanged = (networks: Networks) => {
    this.setState({ networks })
  }

  componentDidMount() {
    Store.subscribe(this.networksChanged)
    Store.getNetworks()
  }

  componentWillUnmount() {
    Store.unsubscribe(this.networksChanged)
  }

  handleDetailClick = (href: string) => {
    Store.getNetworksDetail(href)
      .then((networksDetail: Networks) => {
        this.setState({ networksDetail: networksDetail.network })
        // this.publish(networks)
      })
      .catch(err => {})
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={props => <NetworksTable click={this.handleDetailClick} />}
          />
          <Route
            exact
            path="/networkdetail/:name"
            render={props => (
              <NetworkDetailTable
                click={this.handleDetailClick}
                networksDetail={this.state.networksDetail}
              />
            )}
          />
        </Switch>
      </Router>
    )
  }
}

export default App
