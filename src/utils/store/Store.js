// @flow

/* Copyright (C) Ricardo Costa - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Costa <ricardo.simoescosta@hotmail.com>, July 2018
 */

 
import { BASE_URL, API_ENDPOINTS } from '../constants'
import type { Networks, Network } from '../types'

let instance = null

class Store {

  subscriptions: Array<(networks: Networks) => void> = []
  networks: Networks = {
    networks: [

    ]
  }
  networksDetail: Network = {}

  constructor() {

    if (instance !== null) {
      return instance
    }

    instance = this
    return instance
  }



  getNetworksDetail(href: string) {
    return fetch(BASE_URL + href, {
      method: 'GET',
    }).then((res) => {
      if (res.ok) {
        return res.json()
      }
      return Promise.reject(Error('NOT OK'))
    })
  }

  getNetworks() {
    if (this.networks.networks.length > 0) {
      console.log('SENT FROM CACHE')
      return Promise.resolve(this.networks)
    }

    return fetch(API_ENDPOINTS.NETWORKS, {
      method: 'GET',
    }).then((res) => {
      if (res.ok) {
        return res.json()
      }
      return Promise.reject(Error('NOT OK'))
    }).then((networks: Networks) => {
      this.publish(networks)
    }).catch((err) => {
    })
  }

  subscribe(callback: (networks: Networks) => void) {
    this.subscriptions.push(callback)
    callback(this.networks)
  }

  unsubscribe(callback: (networks: Networks) => void) {
    const index = this.subscriptions.indexOf(callback)
    if (index > -1) {
      this.subscriptions.splice(index, 1)
    }
  }

  publish(networks: Networks) {
    this.networks = networks
    this.subscriptions.forEach(callback => callback(networks))
  }
}

export default new Store()
