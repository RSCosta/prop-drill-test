// @flow



/* Copyright (C) Ricardo Costa - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Costa <ricardo.simoescosta@hotmail.com>, July 2018
 */



export type Networks = {
  networks: Array<Network>
}


export type Network = {
  company: Array<string>,
  href: string,
  id: string,
  location: Location,
  name: string
}


export type NetWorkDetail = {
  company: Array<string>,
  href: string,
  id: string,
  location: Location,
  name: string,
  stations: Array<Stations>
}

type Stations = {
  empty_slots: number,
  extra: Extra,
  free_bikes: number,
  id: string,
  latitude: number,
  longitude: number,
  name: string,
  timestamp: string
}


type Extra = {
  uid: number
}

type Location = {
  city: string,
  country: string,
  latitude: number,
  longitude: number
}