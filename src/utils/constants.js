export const BASE_URL = 'http://api.citybik.es'

export const API_ENDPOINTS = {
  NETWORKS: `${BASE_URL}/v2/networks`,
  SPECIFIC_NETWORK: `${BASE_URL}/v2/networks/`,
}
